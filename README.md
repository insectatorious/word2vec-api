## Word2Vec REST API

This repository implements a basic REST API for read-only interactions with a `word2vec` model.

### Requirements

- Docker
- Trained Word2Vec model that is Gensim compatible ([see here](https://radimrehurek.com/gensim/models/word2vec.html))

### Usage

A public image is hosted at Docker Hub and can be used directly.

    docker pull insectatorious/word2vec-api

Alternatively, the included `Dockerfile` will setup the image as needed.

    docker build . -t $USER/word2vec-api:latest
    docker run --rm -p 8080:8080 -v /path/to/word2vec_model:/models/ $USER/word2vec-api:latest

Once the container is running, it is recommended to run the `tests.py` script which will ensure the API endpoints are operating properly.
See the section on Testing below for more information.
Note: the container expects the trained word2vec model to be in a file named `word2vec_model_all`.
This means that in the command above, `/path/to/word2vec_mode/` should be a directory with the trained model in it.

## Endpoints

The REST API offers three endpoints:

- `/vectors/<word>`
- `/algebra?add=king%2Cwoman&subtract=man`
- `/words`

For each of these, the `version` of the API must be used.
An example call:

    http://0.0.0.0:8080/v1/vectors/king
   
will return the vector for the word *king*.

### Endpoint `/vectors/<word>`

Returns the vector for `<word>` if it exists in the model's vocabulary, otherwise returns a `400 Bad Request`.

**GET vector for a word**

    /v1/vectors/king

returns

    {
    "status": 200,
    "data": [{
        "word": "king", 
        "vector_2d": "[-0.4415754973888397,-0.0484246090054512]", 
        "vector": "[-0.16655705869197845,-0.12326771765947342, .... ,-0.2899991571903229]"}], 
    }


The 2-D vector is generated using [Principal Component Analysis](http://scikit-learn.org/stable/modules/decomposition.html#pca).

**GET vector for a word outside the model's vocabulary**

    /v1/vectors/king2099

returns
    
    {
    "status": 400,
    "message": "Word 'king2099' not found in model vocabulary"
    }


### Endpoint `/algebra?add=king%2Cwoman&subtract=man`

Use query parameter `add` and/or `subtract` to provide a URL encoded comma separated list of words. 

    /v1/algebra?add=king%2Cwoman&subtract=man
    
returns

    {
        "add": [ "king", "woman" ],
        "subtract": [ "man" ],
        "result": [
            [
                "queen",
                0.7367875576019287
            ]
        ]
    }        

**Only `add`**

    /v1/algebra?add=banana%2Capple
    
returns

    {"subtract": [], "result": [["cheese", 0.7728557586669922]], "add": ["banana", "apple"]}    

**Only `subtract`**

    /v1/algebra?subtract=banana%2Capple
    
returns

    {"subtract": ["banana", "apple"], "result": [["mary_windsor", 0.205691397190094]], "add": []}    

### Endpoint `/words`

This endpoint returns all the words in the model's vocabulary.
No pagination is implemented so be wary if the model's vocabulary is large.
An operation (for one of the other endpoints) using a word outside this set will return a `400 Bad Request`.

    /v1/words

returns

    {"words": ["jump", "lonely", "kael", "paradise_falls", "split", ..., "agony", "religion", "tigers", "memory"],
     "status": 200}

## Development

### Mount source inside container

To keep the API running without rebuilding the container after each change, make the following changes:

- Add `--reload` to the `CMD` in `Dockerfile`. This will reload the API when the source changes.
- Mount the source inside the container using an additional `-v` to the command specified above: `-v "$(pwd)":/usr/src/app`

### Unit tests

A basic set of unit tests are provided in `tests.py`.
They depend on the `requests` library (which can be installed via `pip install requests`).
Once the docker container is running, open `tests.py` and edit the `host` and `port` values to match the running container.
Then, to run the tests:

    python tests.py