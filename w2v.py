from functools import lru_cache

from gensim.models import Word2Vec

print("Loading model...")
word2vec_filepath = "/models/word2vec_model_all"
model_full = Word2Vec.load(word2vec_filepath)
model =  model_full.wv
del model_full
index2word_set = set(model.wv.index2word)
print("Model loaded.")

@lru_cache(maxsize=128)
def get_word_vector(word):
  if word in index2word_set:
    return model[word]
  else:
    return None

@lru_cache(maxsize=32)
def get_related_terms(token, topn=10):
  """
  look up the topn most similar terms to token
  and print them as a formatted list
  """

  return model.most_similar(positive=[token], topn=topn)

def word_algebra(add, subtract, topn=1):
  """
  combine the vectors associated with the words provided
  in add= and subtract=, look up the topn most similar
  terms to the combined vector, and print the result(s)
  """
  add_missing = [word for word in add if word not in index2word_set]
  subtract_missing = [word for word in subtract if word not in index2word_set]
  if add_missing or subtract_missing:
    raise ValueError("Words not in vocabulary: {}".format(
      ','.join(add_missing + subtract_missing)))

  return model.most_similar(positive=add, negative=subtract, topn=topn)
